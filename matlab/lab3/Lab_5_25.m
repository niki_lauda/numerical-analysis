function result = Lab_5_25(flag)
    func = @(x) x.*x.*sqrt(x) - sinh(x)/exp(x);
    result = 0;
        if flag == 1
            result = integral(func, 13, 29);
			fplot(func);
            sprintf('Integral %16.f',result);
        end
end
