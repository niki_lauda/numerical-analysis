function n = findIntervals()
    func = @(x) 15 * sqrt(x)/4 - 2*exp(-x)*sinh(x) + 2*exp(-x)*cosh(x);
    max_x = fminbnd(@(x) -func(x), 13, 29);
    fplot(func, [13 29]);
    h = sqrt((10^-11 * 12)/((29-13) * func(max_x)));
    n = (29 - 13)/h;
    sprintf('Number of intervals %16.f',n)
end
