x = [
	2.211
	2.228
	2.306
	3.686
	4.054
	4.200
	4.489
	5.039
	5.637
	5.700
	6.394
	7.350
	11.072
	11.565
	11.787
	12.144
	12.452
	13.182
	14.168
	14.389
	15.763
	15.831
	16.034
	17.760
	21.303
	22.334
	23.865
];

y = [
	3.755
	3.745
	3.701
	4.977
	6.513
	7.291
	9.069	
	13.085
	17.571
	18.000
	21.538
	21.533
	30.782
	37.868
	40.788
	44.721
	47.097
	48.077
	40.447
	38.106
	31.050
	31.333
	32.618
	62.628
	52.033
	51.658
	86.156
]

keys = [
	3.322
	4.376
	5.630
	9.011
	11.485
	12.175
	12.428
	13.922
	15.205
	15.260
	19.718
	21.245
	22.015
]

interpolation = interp1(x, y, keys);

for i = 1 : size(interpolation)
	disp(interpolation(i));
end

clc


V = vander(x);
A = V\y;
results = polyval(A, keys);

for i = 1 : size(results)
	disp(results(i));
end


	
