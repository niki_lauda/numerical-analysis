func = @(x) x*x*sqrt(x) - sinh(x)/exp(x);

function result = Lab_5_25(flag)
result = 0;
	if flag == 1
		display('Hoo')
	end
end
x = zeros(10,1);
y = zeros(10,1);
keys = [ 
	0.280 
	1.257 
	1.409 
	2.048 
	2.050 
	3.439 
	4.507 
	4.913 
	4.986 
	5.517 
	5.815 
	6.330 
	7.762 
	7.789 
	7.885 
]

step = 1;
x(1) = 0.2;
for i = 2 : size(x)
	x(i) = x(i-1) + step;
end

for i = 1 : 10
	y(i) = func(x(i));
end
	
	
for i = 1 : 10
	display(y(i));
end
	
interpolation = interp1(x, y, keys);

for i = 1 : size(interpolation)
	disp(interpolation(i));
end

clc

V = vander(x);
A = V\y;
results = polyval(A, keys);

for i = 1 : size(results)
	disp(results(i));
end
