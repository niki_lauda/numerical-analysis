﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ASPLabs.ModelViews.Interpolation;
using ASPLabs.LabsLogic.Interpolation;
namespace ASPLabs.Controllers
{
    public class InterpolationController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View("InputForm");
        }

        public IActionResult Lagrange(InterpolationInput ii)
        {
            List<double> xValues;
            List<double> yValues;
            List<double> controlPoints;
            try
            {
                xValues = VectorParser.ParseFromString(ii.xValues);
                yValues = VectorParser.ParseFromString(ii.yValues);
                controlPoints = VectorParser.ParseFromString(ii.controlPoints);
            }
            catch (ArgumentNullException e)
            {
                return View("ERROR! Invalid arguments!");
            }

            Lagrange lg = new Lagrange(xValues, yValues, controlPoints);

            InterpolationOutput io = new InterpolationOutput();
            io.xKeys = controlPoints;
            io.yValues = lg.CalculateAll();

            ViewBag.Title = "Lagrange Interpolation";
            ViewBag.methodName = "Lagrange polynomial";
            ViewBag.description = "In numerical analysis, Lagrange polynomials are used for polynomial interpolation";
            ViewBag.xValues = xValues;
            ViewBag.yValues = yValues;
            lg.initLagrangePoints();
            ViewBag.xLagrange = lg.xLagrange;
            ViewBag.yLagrange = lg.yLagrange;

            return View("Lagrange", io);
        }

        public IActionResult Gauss(InterpolationInput ii)
        {
            List<double> controlPoints;
            double step = 0;
            try
            {
                controlPoints = VectorParser.ParseFromString(ii.controlPoints);
            }
            catch (ArgumentNullException e)
            {
                return View("ERROR! Invalid arguments!");
            }

            Gauss st = new Gauss(controlPoints);
            st.CalculateAll();

            InterpolationOutput io = new InterpolationOutput();
            io.xKeys = controlPoints;
            io.yValues = st.CalculateAll();


            ViewBag.xValues = st._xValues;
            ViewBag.yValues = st._yValues;
            st.initGaussPoints();
            ViewBag.xGauss = st.xGauss;
            ViewBag.yGauss = st.yGauss;

            ViewBag.Title = "Gauss Interpolation";
            ViewBag.methodName = "Gauss Forward/Back polynomial";
            ViewBag.description = "Gaussian process is a powerful non-linear interpolation tool. Many popular interpolation tools are actually equivalent to particular Gaussian processes";

            return View(io);
        }

        public IActionResult Stirling(InterpolationInput ii)
        {
            List<double> controlPoints;
            double step = 0;
            try
            {
                controlPoints = VectorParser.ParseFromString(ii.controlPoints);
            }
            catch (ArgumentNullException e)
            {
                return View("ERROR! Invalid arguments!");
            }

            Stirling st = new Stirling(controlPoints);
            st.CalculateAll();

            InterpolationOutput io = new InterpolationOutput();
            io.xKeys = controlPoints;
            io.yValues = st.CalculateAll();

            ViewBag.xValues = st._xValues;
            ViewBag.yValues = st._yValues;
            st.initStirlingPoints();
            ViewBag.xStirling = st.xStirling;
            ViewBag.yStirling = st.yStirling;

            ViewBag.Title = "Stirling Interpolation";
            ViewBag.methodName = "Stirling Interpolation";
            ViewBag.description = "Stirling’s formula is used for the interpolation of functions for values of x close to one of the middle nodes a";

            return View(io);
        }

        public IActionResult NewtonMethod(InterpolationInput ii)
        {
            List<double> controlPoints;
            double step = 0;
            try
            {
                controlPoints = VectorParser.ParseFromString(ii.controlPoints);
            }
            catch (ArgumentNullException e)
            {
                return View("ERROR! Invalid arguments!");
            }

            Newton nt = new Newton(controlPoints);


            InterpolationOutput io = new InterpolationOutput();
            io.xKeys = controlPoints;
            io.yValues = nt.CalculateAll();

            ViewBag.Title = "Newton Interpolation";
            ViewBag.methodName = "Newton Interpolation First Formula";
            ViewBag.description = "The Newton polynomial is sometimes called Newton's divided differences interpolation polynomial because the coefficients of the polynomial are calculated using divided differences";
            ViewBag.xValues = nt._xValues;
            ViewBag.yValues = nt._yValues;
            nt.initNewtonPoints();
            ViewBag.xNewton = nt.xNewton;
            ViewBag.yNewton = nt.yNewton;

            return View("NewtonMethod", io);
        }
    }
}
