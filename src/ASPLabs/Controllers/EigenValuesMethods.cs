﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.AspNet.Mvc;
using ASPLabs.LabsLogic;
using ASPLabs.ModelViews;
using ASPLabs.LabsLogic.PowerMethod;
using ASPLabs.LabsLogic.Methods;
// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ASPLabs.Controllers
{
    public class EigenValuesMethods : Controller
    {
        [HttpGet]
        public IActionResult PowerMethod()
        {
            ViewBag.Title = "Matrix Input";
            ViewBag.Action = "PowerMethodCalcs";
            return View("InputMatrix");
        }


        public IActionResult PowerMethodCalcs(MatrixInput modelView)
        {
            List<List<double>> matrix = MatrixParser.ParseFromString(modelView.MatrixString);
            PowerMethod pm = new PowerMethod(matrix, modelView.Accuracy, 0.4);
            pm.Execute();
            ViewBag.results = pm.ExecutionResults;
            ViewBag.Title = "Power Method";
            ViewBag.methodName = "Power Method";
            ViewBag.description = "Finding eigenvalues and eigenvectors using Power Method №1";
            return View("MethodCalcsOutput", pm.ExecutionResults);
        }

        public IActionResult ScalarProductMethod()
        {
            ViewBag.Title = "Matrix Input";
            ViewBag.Action = "ScalarProductMethodCalcs";
            return View("InputMatrix");
        }

        public IActionResult ScalarProductMethodCalcs(MatrixInput modelView)
        {
            List<List<double>> matrix = MatrixParser.ParseFromString(modelView.MatrixString);
            ScalarProductMethod sp = new ScalarProductMethod(matrix, modelView.Accuracy, 0.4);
            sp.Execute();
            ViewBag.results = sp.ExecutionResults;
            ViewBag.Title = "SP Method";
            ViewBag.methodName = "Scalar Product Method";
            ViewBag.description = "Finding eigenvalues and eigenvectors using Scalar Product Method №1";
            return View("MethodCalcsOutput", sp.ExecutionResults);
        }

        public IActionResult PowerAitkenMethod()
        {
            ViewBag.Title = "Matrix Input";
            ViewBag.Action = "PowerAitkenMethodCalcs";
            return View("InputMatrix");
        }

        public IActionResult PowerAitkenMethodCalcs(MatrixInput modelView)
        {
            List<List<double>> matrix = MatrixParser.ParseFromString(modelView.MatrixString);
            PowerAitkenMethod pa = new PowerAitkenMethod(matrix, modelView.Accuracy, 0.2);
            pa.Execute();
            ViewBag.results = pa.ExecutionResults;
            ViewBag.Title = "PA Method";
            ViewBag.methodName = "Power Aitken Method";
            ViewBag.description = "Finding eigenvalues and eigenvectors using Power Method №1 with Aitken process";
            return View("MethodCalcsOutput", pa.ExecutionResults);
        }
    }
}
