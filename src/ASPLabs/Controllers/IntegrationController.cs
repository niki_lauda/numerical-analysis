﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ASPLabs.LabsLogic.Integration;
using ASPLabs.ModelViews.Integration;

namespace ASPLabs.Controllers
{
    public class IntegrationController : Controller
    {
        public IActionResult Index()
        {
            return View("InputForm");
        }

        public IActionResult Trapezoidal(IntegrationInput ii)
        {
            Trapezoidal trapezoidal = new Trapezoidal(ii.IntervalsCount);
            trapezoidal.initPoints();
            trapezoidal.CalcIntegral();

            ViewBag.xValues = trapezoidal.DrawPointsX;
            ViewBag.yValues = trapezoidal.DrawPointsY;
            ViewBag.Integral = trapezoidal.Integral;
            ViewBag.Title = "Trapezoidal Integration";
            ViewBag.methodName = "Trapezoidal Integration";
            ViewBag.description = "In mathematics, and more specifically in numerical analysis," +
                "the trapezoidal rule (also known as the trapezoid rule or trapezium rule) is a technique for approximating the definite integral";

            return View();
        }

        public IActionResult SquareTrapezoidal(IntegrationInput ii)
        {
            SquareTrapezoidal sqrTrap = new SquareTrapezoidal(ii.IntervalsCount);
            sqrTrap.Accuracy = ii.Accuracy;
            sqrTrap.initPoints();
            sqrTrap.CalcIntegral();

            ViewBag.xValues = sqrTrap.DrawPointsX;
            ViewBag.yValues = sqrTrap.DrawPointsY;
            ViewBag.Integral = sqrTrap.Integral;

            ViewBag.IT_h = sqrTrap.I_Th;
            ViewBag.ITH = sqrTrap.I_TH[0];
            ViewBag.IPH = sqrTrap.I_PH;
            ViewBag.RTh = sqrTrap.R_h;

            ViewBag.Title = "Rectangle Trapezoidal Integration";
            ViewBag.methodName = "Rectangle Trapezoidal Integration";

            return View();
        }
    }
}
