﻿using System;
using System.Collections.Generic;

namespace ASPLabs.LabsLogic
{
    public class Matrix<T> where T :System.IComparable<T>
    {
        private List<List<T>> _matrix;

        public Matrix(int rows = 0, int columns = 0)
        {
            _matrix = new List<List<T>>(rows);
            for(int i = 0; i < _matrix.Count; i++)
            {
                _matrix[i] = new List<T>();
            }
        }

        public Matrix(List<List<T>> matrix)
        {
            _matrix = matrix;
        }

        public int Rows
        {
            get
            {
                return _matrix.Count;
            }
        }

        public int Columns
        {
            get
            {
                if (_matrix.Count > 0)
                {
                    return _matrix[0].Count;
                }
                return 0;
            }
        }

        public List<T> this[int i]
        {
            get
            {
                if (i < _matrix.Count)
                {
                    return _matrix[i];
                }
                throw new IndexOutOfRangeException($"Current matrix has {Rows} rows. Index {i} is out of range.");
            }
            set
            {
                if (i < _matrix.Count)
                {
                    _matrix[i] = value;
                }
                throw new IndexOutOfRangeException($"Current matrix has {Rows} rows. Index {i} is out of range.");
            }
        }
    }
}
