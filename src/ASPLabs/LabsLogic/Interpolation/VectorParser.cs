﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.LabsLogic.Interpolation
{
    public class VectorParser
    {
        public static List<double> ParseFromString(string vectorString)
        {
            if (string.IsNullOrEmpty(vectorString))
            {
                throw new ArgumentNullException("Empty or Null string cannot be parsed into Matrix.");
            }

            List<double> vector = new List<double>();

            foreach (string num in vectorString.Split('\n'))
            {
                vector.Add(double.Parse(num));
            }

            return vector;
        }
    }
}
