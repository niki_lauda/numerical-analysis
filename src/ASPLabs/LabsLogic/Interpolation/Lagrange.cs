﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.LabsLogic.Interpolation
{
    public class Lagrange
    {
        private List<double> _xValues;
        private List<double> _yValues;
        private List<double> _keyValues;

        public List<double> xLagrange { get; set; }
        public List<double> yLagrange { get; set; }

        public Lagrange(List<double> xValues, List<double> yValues, List<double> keyValues)
        {
            _xValues = xValues;
            _yValues = yValues;
            _keyValues = keyValues;
        }

        public void initLagrangePoints()
        {
            xLagrange = new List<double>();
            yLagrange = new List<double>();

            for (double i = _xValues[0]; i < 24; i += 0.01)
            {
                // _xValues[_xValues.Count - 1] + 2 * 0.01
                xLagrange.Add(i);
                yLagrange.Add(CalculateOne(i));
            }
        }

        public List<double> CalculateAll()
        {
            List<double> all = new List<double>();

            foreach (double val in _keyValues)
            {
                all.Add(CalculateOne(val));
            }

            return all;
        }

        //MAIN ALGORITHM
        public double CalculateOne(double xKey)
        {
            double result = 0.0;

            for (int i = 0; i < _xValues.Count; i++)
            {
                double polynom = 1.0;

                for (int k = 0; k < _xValues.Count; k++)
                {
                    if (i == k)
                    {
                        continue;
                    }
                    polynom *= (xKey - _xValues[k]) / (_xValues[i] - _xValues[k]);
                }

                result += _yValues[i] * polynom;
            }

            return result;
        }
    }
}
