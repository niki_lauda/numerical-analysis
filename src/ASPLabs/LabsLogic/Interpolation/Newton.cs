﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.LabsLogic.Interpolation
{
    public class Newton
    {
        public List<double> _xValues;
        public List<double> _yValues;
        public List<double> _keyValues;
        private double _step = 1;
        private List<double> _diffs;
        private List<List<double>> _allDiffs;

        public List<double> xNewton { get; set; }
        public List<double> yNewton { get; set; }

        public Newton(List<double> keyValues)
        {
            _xValues = new List<double>();
            _yValues = new List<double>();
            _keyValues = keyValues;
        }

        public void initNewtonPoints()
        {
            xNewton = new List<double>();
            yNewton = new List<double>();

            for (double x = 0.2; x < 10; x += _step)
            {
                xNewton.Add(x);
                yNewton.Add(CalculateOne(x));
            }
        }


        public List<double> CalculateAll()
        {
            setPoints();

            calcDifferences();

            List<double> all = new List<double>();

            for (int i = 0; i < _keyValues.Count; i++)
            {
                all.Add(CalculateOne(_keyValues[i]));
            }

            return all;
        }

        private void calcDifferences()
        {
            _allDiffs = new List<List<double>>();
            _diffs = new List<double>();

            List<double> diffsPartial = new List<double>();

            List<double> prevDiffs = _yValues;
            _diffs.Add(_yValues[0]);
            for (int i = 0; i < _yValues.Count - 1; i++)
            {
                for (int k = 1; k < prevDiffs.Count; k++)
                {
                    diffsPartial.Add(prevDiffs[k] - prevDiffs[k - 1]);
                }
                _allDiffs.Add(diffsPartial);
                _diffs.Add(diffsPartial[0]);
                prevDiffs = diffsPartial;
                diffsPartial = new List<double>();
            }
        }

        public double func(double x)
        {
            double sin = Math.Sin(x * x);
            double exp = Math.Exp(x);
            double ln = Math.Log(x);
            double ch = (Math.Exp(x) + Math.Exp(-x)) / 2;
            return sin + exp + ln + ch;
        }

        //MAIN ALGORITHM
        public double CalculateOne(double x)
        {
            double result = _diffs[0];
            double q = (x - _xValues[0]) / _step;

            for (int i = 1; i < _diffs.Count; i++)
            {
                double part = 1;
                for(int j = 0; j < i; j++)
                {
                    part *= q - j;
                }
                part *= _diffs[i] / Factorial(i);
                result += part;
            }
            return result;
        }

        public void setPoints()
        {
            for (double x = 0.2; x < 10; x += _step)
            {
                _xValues.Add(x);
                _yValues.Add(func(x));
            }
        }

        private double Factorial(int i)
        {
            if (i <= 1)
                return 1;
            return i * Factorial(i - 1);
        }
    }
}
