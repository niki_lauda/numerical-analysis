﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.LabsLogic.Interpolation
{
    public class Stirling
    {
        public List<double> _xValues;
        public List<double> _yValues;
        private List<double> _keyValues;
        private List<List<double>> _diffs;
        private List<double> _diffForward;
        private List<double> _diffBack;
        private double _step = 1;


        public List<double> xStirling { get; set; }
        public List<double> yStirling { get; set; }

        public Stirling(List<double> keyValues)
        {

            _keyValues = keyValues;
        }
        public void initStirlingPoints()
        {
            xStirling = new List<double>();
            yStirling = new List<double>();

            for (double x = 0.2; x < 10; x += _step)
            {
                xStirling.Add(x);
                yStirling.Add(CalculateOne(x));
            }
        }
        public List<double> CalculateAll()
        {
            _xValues = new List<double>();
            _yValues = new List<double>();

            setPoints();

            List<double> all = new List<double>();

            for (int i = 0; i < _keyValues.Count; i++)
            {
                _diffs = new List<List<double>>();
                _diffForward = new List<double>();
                _diffBack = new List<double>();
                all.Add(CalculateOne(_keyValues[i]));
            }

            return all;
        }

        public void setPoints()
        {
            for (double x = 0.2; x < 9; x += _step)
            {
                _xValues.Add(x);
                _yValues.Add(func(x));
            }
        }

        public double func(double x)
        {
            double sin = Math.Sin(x * x);
            double exp = Math.Exp(x);
            double ln = Math.Log(x);
            double ch = (Math.Exp(x) - Math.Exp(-x)) / 2;
            return sin + exp + ln + ch;
        }

        private void calcDiffsForward()
        {
            int count = _yValues.Count;

            List<double> values = _yValues;

            for (int i = 0; i < count - 1; i++)
            {
                values = calcDiffPart(values);
                _diffs.Add(values);
                double val;
                val = _diffs[i][_diffs[i].Count / 2];
                _diffForward.Add(val);
            }
        }

        private void calcDiffsBack()
        {
            int count = _yValues.Count;

            List<double> values = _yValues;

            for (int i = 0; i < count - 1; i++)
            {
                values = calcDiffPart(values);
                _diffs.Add(values);
                double val;
                if (_diffs[i].Count % 2 == 1)
                {
                    val = _diffs[i][_diffs[i].Count / 2];
                }
                else
                {
                    val = _diffs[i][_diffs[i].Count / 2 - 1];
                }
                _diffBack.Add(val);
            }
        }

        private List<double> calcDiffPart(List<double> elems)
        {
            List<double> _diffPart = new List<double>();

            for (int i = 1; i < elems.Count; i++)
            {
                _diffPart.Add(elems[i] - elems[i - 1]);
            }

            return _diffPart;

        }

        //MAIN ALGORITHM
        public double CalculateOne(double x)
        {
            double middle = _xValues[_xValues.Count / 2];

            int middleIndex = _xValues.Count / 2;

            double q = (x - middle) / _step;
            int sign = 0;
            calcDiffsForward();
            calcDiffsBack();

            return StirlingAlgo(middle, middleIndex, q, sign);
        }

        private double StirlingAlgo(double middleVal, int middleIndex, double q, int sgn)
        {
            double result = 0;


            result += _yValues[middleIndex];
            int productCount = 0;

            for (int i = 0; i < _diffForward.Count; i++)
            {
                double partial = 1;

                int value = 0;
                partial = q;

                if (i % 2 == 1)
                {
                    partial *= q;
                }
                if(i % 2 == 0 && i > 1)
                {
                    productCount += 1;
                }

                for (int j = 0; i > 1 && j < productCount; j++)
                {
                    value += 1;

                    partial *= q * q - Math.Pow(value, 2);
                }
                partial /= Factorial(i + 1);


                partial *= (_diffForward[i] + _diffBack[i]) / 2;

                result += partial;
            }

            return result;
        }

        private double Factorial(int i)
        {
            if (i <= 1)
                return 1;
            return i * Factorial(i - 1);
        }
    }
}
