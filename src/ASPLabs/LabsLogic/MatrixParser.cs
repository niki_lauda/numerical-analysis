﻿using System;
using System.Linq;
using System.Collections.Generic;
using ASPLabs.LabsLogic;

namespace ASPLabs.LabsLogic
{
    public static class MatrixParser
    {
        public static List<List<double>> ParseFromString(string matrixString)
        {
            if (string.IsNullOrEmpty(matrixString))
            {
                throw new ArgumentNullException("Empty or Null string cannot be parsed into Matrix.");
            }

            List<List<double>> matrix = new List<List<double>>();

            foreach (string row in matrixString.Split('\n'))
            {
                matrix.Add(new List<double>(row.Split(' ').OfType<string>().Select(num => double.Parse(num)).ToArray()));
            }

            return matrix;
        }
    }
}
