﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.LabsLogic.Integration
{
    public class SquareTrapezoidal
    {
        private int _intervalsCount;
        private double _a;
        private double _b;
        private double _h;

        public double Integral { get; set; }

        public double Accuracy { get; set; }


        public List<double> PointsX { get; set; }
        public List<double> PointsY { get; set; }

        public List<double> I_TH { get; set; }
        public List<double> I_PH { get; set; }
        public List<double> I_Th { get; set; }
        public List<double> R_h { get; set; }


        public List<double> DrawPointsX { get; set; }
        public List<double> DrawPointsY { get; set; }

        public SquareTrapezoidal(int intervalsCount)
        {
            _intervalsCount = intervalsCount;
            _a = 13;
            _b = 29;
            _h = (_b - _a) / intervalsCount;

            PointsX = new List<double>();
            PointsY = new List<double>();
            DrawPointsX = new List<double>();
            DrawPointsY = new List<double>();

            I_TH = new List<double>();
            I_PH = new List<double>();
            I_Th = new List<double>();
            R_h = new List<double>();
        }

        public void initPoints()
        {
            for (double x = _a; x <= _b; x += 1)
            {
                DrawPointsX.Add(x);
                DrawPointsY.Add(func(x));
            }
        }

        public void CalcIntegral()
        {
            double res = 0;

            int n = 1;
            double H = _b - _a;
            double I_t = (H / 2) * (func(_a) + func(_b));
            I_TH.Add(I_t);
            while (true)
            {
                PointsX.Clear();
                PointsY.Clear();
                double h = H / 2;

                PointsX.Add(_a + h);

                for (int i = 1; i < n; i++)
                {
                    PointsX.Add(PointsX[i - 1] + H);
                }
                for (int i = 0; i < n; i++)
                {
                    PointsY.Add(func(PointsX[i]));
                }


                double sum = 0;
                for (int i = 0; i < PointsY.Count; i++)
                {
                    sum += PointsY[i];
                }
                double I_p = H * sum;
                I_PH.Add(I_p);

                double I_th = (I_p + I_t) / 2;
                I_Th.Add(I_th);

                double R_th = (I_th - I_t) / 3;
                R_h.Add(R_th);

                if (Math.Abs(R_th) > Accuracy)
                {
                    n = 2 * n;
                    H = h;
                    I_t = I_th;
                }
                else
                {
                    Integral = I_th + R_th;
                    return;
                }
            }

        }

        private double func(double x)
        {
            double one = x * x * Math.Sqrt(x);
            double sh = (Math.Exp(x) - Math.Exp(-x)) / 2;
            double e = Math.Exp(x);
            double sum = one - (sh / e);
            return sum;
        }
    }
}
