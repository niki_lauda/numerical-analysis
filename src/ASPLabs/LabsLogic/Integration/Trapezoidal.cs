﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.LabsLogic.Integration
{
    public class Trapezoidal
    {
        private int _intervalsCount;
        private double _a;
        private double _b;
        private double _h;

        public double Integral { get; set; }

        public List<double> PointsX { get; set; }
        public List<double> PointsY { get; set; }


        public List<double> DrawPointsX { get; set; }
        public List<double> DrawPointsY { get; set; }

        public Trapezoidal(int intervalsCount)
        {
            _intervalsCount = intervalsCount;
            _a = 13;
            _b = 29;
            _h = (_b - _a) / intervalsCount;

            PointsX = new List<double>();
            PointsY = new List<double>();
            DrawPointsX = new List<double>();
            DrawPointsY = new List<double>();
        } 

        public void initPoints()
        {
            for(double x = _a; x <= _b; x += _h)
            {
                PointsX.Add(x);
                PointsY.Add(func(x));
            }

            for (double x = _a; x <= _b; x += 1)
            {
                DrawPointsX.Add(x);
                DrawPointsY.Add(func(x));
            }
        }

        public void CalcIntegral()
        {
            double sum = (PointsY[0] + PointsY[PointsY.Count - 1])/2;

            for(int i = 1; i < PointsX.Count - 1; i++)
            {
                sum += PointsY[i];
            }

            Integral = _h * sum;
        }

        private double func(double x)
        {
            double one = x * x * Math.Sqrt(x);
            double sh = (Math.Exp(x) - Math.Exp(-x))/2;
            double e = Math.Exp(x);
            double sum = one - (sh / e);
            return sum;
        }
    }
}
