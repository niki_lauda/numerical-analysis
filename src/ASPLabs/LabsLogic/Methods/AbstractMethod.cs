﻿using ASPLabs.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.LabsLogic.Methods
{
    public abstract class AbstractMethod
    {
        protected List<List<double>> _matrix;

        protected double _accuracy;

        protected double _delta;

        public MethodResults ExecutionResults { get; set; }

        public AbstractMethod(List<List<double>> matrix, double accuracy, double delta)
        {
            _matrix = matrix;
            _accuracy = accuracy;
            _delta = delta;
        }

        public abstract void Execute();
    }
}
