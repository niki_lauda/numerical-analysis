﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASPLabs.ModelViews;
using ASPLabs.LabsLogic.Methods;
using System.Diagnostics;
using System.Threading;

namespace ASPLabs.LabsLogic.PowerMethod
{
    public class PowerMethod : AbstractMethod
    {
        public PowerMethod(List<List<double>> matrix, double accuracy = 0.05, double delta = 0) : base(matrix, accuracy, delta){}

        //Important!!!
        //http://fan-5.ru/better/article-196154.php
        public override void Execute()
        {
            Stopwatch sw = new Stopwatch();
            MethodResults results = new MethodResults();
            results.matrix = _matrix;
            results.Epsilon = _accuracy;
            List<double> yPrev = new List<double>();
            for (int i = 0; i < _matrix.Count; i++)
            {
                yPrev.Add(1);
            }
            List<double> zPrev = MathOps.divideVector(yPrev, MathOps.getModule(yPrev));
            List<double> y = new List<double>();
            List<double> lambdas = new List<double>();
            List<double> lambdasPrev = new List<double>();
            List<int> Sindexes = new List<int>();
            double lambdaPrev = 0.0, lambda = 0.0;
            int p = 2;
            sw.Start();
            bool EndFlag = false;
            for (int k = 0; ; k++, lambda = 0, yPrev = y, lambdasPrev = lambdas, lambdas = new List<double>())
            {
                y = MathOps.MatrixVectorProduct(_matrix, zPrev);
                //because of this
                for(int i = 0; i < zPrev.Count; i++)
                {
                    if(Math.Abs(zPrev[i]) > _delta)
                    {
                        lambdas.Add(y[i] / zPrev[i]);
                    }
                }
                zPrev = MathOps.divideVector(y, MathOps.getModule(y));
                EndFlag = true;
                if(lambdasPrev.Count == 0 || lambdasPrev.Count != lambdas.Count)
                {
                    lambdasPrev.Clear();
                    for(int i = 0; i < lambdas.Count; i++)
                    {
                        lambdasPrev.Add(0);
                    }
                }
                for (int i = 0; i < lambdas.Count; i++)
                {
                   if(Math.Abs(lambdas[i] - lambdasPrev[i]) > _accuracy)
                    {
                        EndFlag = false;
                    }
                }
                for (int i = 0; i < lambdas.Count; i++)
                {
                    lambda += lambdas[i];
                }
                lambda = lambda / lambdas.Count;
                if (EndFlag)
                {
                    sw.Stop();
                    results.lambdas.Add(lambda);
                    results.stepVectorsY.Add(y);
                    results.ExecutionTime = sw.Elapsed.ToString();
                    results.IterationsCount = ++k;
                    results.PairsList.Add(new EigenPair() { eigenValue = lambda, eigenVector = zPrev });
                    ExecutionResults = results;
                    break;
                }
                results.lambdas.Add(lambda);
                results.stepVectorsY.Add(y);
            }
        }
    }
}
