﻿using ASPLabs.ModelViews;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.LabsLogic.Methods
{
    public class ScalarProductMethod:AbstractMethod
    {
        public ScalarProductMethod(List<List<double>> matrix, double accuracy = 0.05, double delta = 0) : base(matrix, accuracy, delta){ }

        public override void Execute()
        {
            Stopwatch sw = new Stopwatch();
            MethodResults results = new MethodResults();
            results.matrix = _matrix;
            results.Epsilon = _accuracy;
            List<double> yPrev = new List<double>();
            for (int i = 0; i < base._matrix.Count; i++)
            {
                yPrev.Add(1);
            }
            double s = MathOps.scalarProduct(yPrev, yPrev);
            double t = 0.0;
            double p = Math.Sqrt(s);
            List<double> z = MathOps.divideVector(yPrev, p);
            double lambdaPrev = 0.0, lambda = 0.0;
            List<double> y = new List<double>();
            double Epsilon = 0.05;
            sw.Start();
            for (int k = 0 ;; k++, yPrev = y, lambdaPrev = lambda)
            {
                y = MathOps.MatrixVectorProduct(_matrix, z);
                s = MathOps.scalarProduct(y, y);
                t = MathOps.scalarProduct(y, z);
                p = Math.Sqrt(s);
                lambda = s / t;
                z = MathOps.divideVector(y, p);
                if (Math.Abs(lambda - lambdaPrev) <= Epsilon)
                {
                    sw.Stop();
                    results.lambdas.Add(lambda);
                    results.stepVectorsY.Add(y);
                    results.ExecutionTime = sw.Elapsed.ToString();
                    results.IterationsCount = ++k;
                    results.PairsList.Add(new EigenPair() { eigenValue = lambda, eigenVector = z });
                    ExecutionResults = results;
                    break;
                }
                results.lambdas.Add(lambda);
                results.stepVectorsY.Add(y);
            }
        }
    }
}
