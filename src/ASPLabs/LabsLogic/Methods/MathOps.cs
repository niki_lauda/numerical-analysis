﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.LabsLogic.Methods
{
    public static class MathOps
    {
        public static List<double> substractVectors(List<double> a, List<double> b)
        {
            List<double> res = new List<double>();
            for (int i = 0; i < a.Count; i++)
            {
                res.Add(a[i] - b[i]);
            }
            return res;
        }

        public static List<double> addVectors(List<double> a, List<double> b)
        {
            List<double> res = new List<double>();
            for (int i = 0; i < a.Count; i++)
            {
                res.Add(a[i] + b[i]);
            }
            return res;
        }

        public static List<double> divideVectors(List<double> a, List<double> b)
        {
            List<double> res = new List<double>();
            for (int i = 0; i < a.Count; i++)
            {
                res.Add(a[i] / b[i]);
            }
            return res;
        }

        public static List<double> multVector(List<double> a, double b)
        {
            List<double> res = new List<double>();
            for (int i = 0; i < a.Count; i++)
            {
                res.Add(a[i] * b);
            }
            return res;
        }

        public static double scalarProduct(List<double> a, List<double> b)
        {
            double res = 0.0f;
            if (a.Count != b.Count)
                return -1;
            for (int i = 0; i < a.Count; i++)
            {
                res += a[i] * b[i];
            }
            return res;
        }

        public static List<double> divideVector(List<double> vector, double dividor)
        {
            List<double> resVector = new List<double>();
            for (int i = 0; i < vector.Count; i++)
            {
                resVector.Add(vector[i] / dividor);
            }
            return resVector;
        }

        public static void printVector(List<double> vector, string name)
        {
            Console.WriteLine("{0} = ({1} {2} {3})", name, vector[0], vector[1], vector[2]);
        }

        public static List<double> MatrixVectorProduct(List<List<double>> A, List<double> vector)
        {
            double res = 0.0;
            List<double> resVector = new List<double>();
            for (int i = 0; i < A[0].Count; i++)
            {
                for (int j = 0; j < A.Count; j++)
                {
                    res += A[j][i] * vector[j];
                }
                resVector.Add(res);
                res = 0.0;
            }
            return resVector;
        }

        public static double getModule(List<double> vector)
        {
            double res = 0.0;
            foreach (double num in vector)
            {
                res += num * num;
            }
            return Math.Sqrt(res);
        }

        public static List<double> PowerVector(List<double> vector, double power)
        {
            List<double> res = new List<double>();
            for(int i = 0; i < vector.Count; i++)
            {
                res.Add(Math.Pow(vector[i], power));
            }
            return res;
        }
    }
}
