﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASPLabs.ModelViews;
using ASPLabs.LabsLogic.Methods;
using System.Diagnostics;
using System.Threading;

namespace ASPLabs.LabsLogic.PowerMethod
{
    public class PowerAitkenMethod : AbstractMethod
    {
        public PowerAitkenMethod(List<List<double>> matrix, double accuracy = 0.05, double delta = 0) : base(matrix, accuracy, delta) { }

        //Important!!!
        public override void Execute()
        {
            Stopwatch sw = new Stopwatch();
            MethodResults results = new MethodResults();
            results.matrix = _matrix;
            results.Epsilon = _accuracy;
            List<double> yPrev = new List<double>();
            for (int i = 0; i < _matrix.Count; i++)
            {
                yPrev.Add(1);
            }
            List<double> zPrev = MathOps.divideVector(yPrev, MathOps.getModule(yPrev));
            List<double> y = new List<double>();
            List<List<double>> lambdas = new List<List<double>>();
            List<double> aitkenslambdas = new List<double>();
            List<int> Sindexes = new List<int>();
            List<List<double>> z = new List<List<double>>();
            double lambdaPrev = 0.0, lambda = 0.0;
            int q = 2;//more that 2 because there are two vectors back substraction
            sw.Start();
            bool EndFlag = false;
            for (int k = 0; ; k++, lambda = 0, yPrev = y)
            {
                lambdas.Add(new List<double>());
                Sindexes.Clear();
                y = MathOps.MatrixVectorProduct(_matrix, zPrev);
                for (int i = 0; i < zPrev.Count; i++)
                {
                    lambdas[k].Add(y[i] / zPrev[i]);

                    if (Math.Abs(zPrev[i]) > _delta)
                    {
                        Sindexes.Add(i);
                    }
                }
                zPrev = y;

                if (k > 1 && k % q == 0)
                {
                    //Aitken Process
                    List<double> zZaehler = MathOps.PowerVector(MathOps.substractVectors(z[k - 1], z[k - 2]), 2);
                    List<double> zNenner = MathOps.substractVectors(MathOps.addVectors(zPrev, z[k - 2]), MathOps.multVector(z[k - 1], 2));
                    zPrev = MathOps.substractVectors(z[k - 2], MathOps.divideVectors(zZaehler, zNenner));

                    var ztemp = new List<double>();
                    for(int i = 0; i < zPrev.Count; i++)
                    {
                        ztemp.Add(z[k-2][i] - Math.Pow(z[k-1][i] - z[k-2][i], 2)/(zPrev[i] - 2 * z[k-1][i]+z[k-2][i]));
                    }
                    int a = 0;
                    a = 3;

                }
                z.Add(zPrev);
                EndFlag = true;
                if (k == 0 || Sindexes.Count == 0) {
                    results.lambdas.Add(lambda);
                    results.stepVectorsY.Add(y);
                    continue;
                };
                for (int i = 0; i < Sindexes.Count && EndFlag; i++)
                {
                    if (Math.Abs(lambdas[k][Sindexes[i]] - lambdas[k - 1][Sindexes[i]]) > _accuracy)
                    {
                        EndFlag = false;
                    }
                }

                if (EndFlag && k >= 2)
                {
                    for (int i = 0; i < Sindexes.Count; i++)
                    {
                        aitkenslambdas.Add(lambdas[k - 2][Sindexes[i]] - Math.Pow(lambdas[k - 1][Sindexes[i]] - lambdas[k - 2][Sindexes[i]], 2) / (lambdas[k][Sindexes[i]] - 2 * lambdas[k - 1][Sindexes[i]] + lambdas[k - 2][Sindexes[i]]));
                        lambda += aitkenslambdas[i];
                    }
                    lambda = lambda / aitkenslambdas.Count;

                    sw.Stop();
                    results.lambdas.Add(lambda);
                    results.stepVectorsY.Add(y);
                    results.ExecutionTime = sw.Elapsed.ToString();
                    results.IterationsCount = ++k;
                    results.PairsList.Add(new EigenPair() { eigenValue = lambda, eigenVector = zPrev });
                    ExecutionResults = results;
                    break;
                }
                results.lambdas.Add(lambda);
                results.stepVectorsY.Add(y);
            }
        }
    }
}
