﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.ModelViews.Integration
{
    public class IntegrationInput
    {
        public int IntervalsCount { get; set; }
        public double Accuracy { get; set; }
    }
}
