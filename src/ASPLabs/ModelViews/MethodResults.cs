﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.ModelViews
{
    public class EigenPair
    {
        public List<double> eigenVector;
        public double eigenValue;
    }

    public class MethodResults
    {
        public List<EigenPair> PairsList = new List<EigenPair>();
        public List<double> lambdas = new List<double>();
        public string ExecutionTime;
        public long IterationsCount;
        public double Epsilon;
        public List<List<double>> stepVectorsY = new List<List<double>>();
        public List<List<double>> matrix;
    }
}
