﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.ModelViews
{
    public class MatrixInput
    {
        public float Accuracy { get; set; }
        public string MatrixString { get; set; }
    }
}
