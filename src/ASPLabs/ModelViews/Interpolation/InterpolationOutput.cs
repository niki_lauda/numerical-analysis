﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.ModelViews.Interpolation
{
    public class InterpolationOutput
    {
        public List<double> xKeys { get; set; }
        public List<double> yValues { get; set; }
    }
}
