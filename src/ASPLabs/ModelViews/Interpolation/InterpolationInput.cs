﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPLabs.ModelViews.Interpolation
{
    public class InterpolationInput
    {
        public string xValues { get; set; }
        public string yValues { get; set; }
        public string controlPoints { get; set; }
        public string step { get; set; }
    }
}
